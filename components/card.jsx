import Link from "next/link";
import { useRouter } from "next/router";

export default function Card({ className, href, title, children }) {
  let active_classes;
  if (useRouter().asPath === href) {
    active_classes = "bg-indigo-50 border-indigo-400 border-b-4";
  } else {
    active_classes = "opacity-75 hover:opacity-100";
  }
  return (
    <Link href={href}>
      <a className={`flex-1 p-2 rounded block ${active_classes} ${className}`}>
        <h2 className="font-bold text-lg">{title}</h2>
        {children}
      </a>
    </Link>
  );
}
