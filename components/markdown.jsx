// micromark(markdown string) -> html string
import { micromark } from "micromark";

// Because React is client side, it is understandable that setting
// innerHTML is something akin to eval and should be avoided.
//
// Here I just want some content available at build time to be
// converted from Markdown to HTML or React's internal representation,
// and dangerouslySetInnerHTML is the only way.
//
// I don't know, is this really less painful than Hugo templates?
export default function Markdown({ content }) {
  return <div dangerouslySetInnerHTML={{ __html: micromark(content) }}></div>;
}
