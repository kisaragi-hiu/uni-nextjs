// A layout component that's intended for wrapping all content.
//
// For example, to do this:
//
//     some common stuff
//     -----------------
//     nav 1 | content
//     nav 2 |
//
// I define the layout in this component (with the children placed at
// where I want the content to be), then use it in pages:
//
// in pages/a.jsx: <Main>a page</Main>
// in pages/b.jsx: <Main>another page</Main>
//
// Then when we switch between pages it looks like we're just
// switching tabs with the URL updating to reflect that.

import Card from "/components/card";

export default function Main({ children }) {
  return (
    <div className="max-w-4xl m-auto mb-24">
      <h1 className="mt-24 mb-2 text-4xl font-bold">
        尼爾：自動人形在PC上的必備模組
      </h1>
      <p>
        尼爾：自動人形很不錯玩，但它的PC版操作上很有問題。雖然它支援滑鼠和鍵盤，但是因為操作上的問題，基本上不用手把玩起來會有點痛苦。
      </p>
      <p>這是一份介紹一個必備模組和一個好用的模組的指南。</p>
      <time
        title="2022-01-04T19:13:12+0900"
        dateTime="2022-01-04T19:13:12+0900"
      >
        2022年1月4號
      </time>
      <hr className="my-4" />
      <div className="">
        <nav className="flex gap-y-4 my-8">
          <Card href="/input" title="操作改善">
            <p>尼爾：自動人形的預設按鍵會令人抓狂。安裝NAIOM可以比較好一點。</p>
          </Card>
          <Card href="/fps" title="效能調整">
            <p>
              FAR (現為 Special K) 可以改善效能，並且提供一個遊戲內的FPS計數器。
            </p>
          </Card>
          <Card href="/about" title="關於">
            <p>這份文件是……?</p>
          </Card>
        </nav>
        <main className="prose prose-a:text-indigo-600 max-w-none">
          {children}
        </main>
      </div>
    </div>
  );
}
