// Note that this has to be a list of strings naming plugins.

module.exports = {
  plugins: [
    "postcss-import",
    "tailwindcss/nesting",
    "tailwindcss",
    "autoprefixer",
  ],
};
