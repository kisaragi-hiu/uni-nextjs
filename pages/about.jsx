import Head from "next/head";
import Main from "/components/main";
import Markdown from "/components/markdown";

export default () => {
  return (
    <>
      <Head>
        <title>關於</title>
        <meta name="description" content="關於這份文件" />
      </Head>
      <Main>
        <Markdown
          content={`
這是寫給110-1商務網站程式設計實作的期末報告用的。

**原始碼** <https://gitlab.com/kisaragi-hiu/uni-nextjs/-/tree/main>
`}
        />
      </Main>
    </>
  );
};
