import Head from "next/head";
import Main from "/components/main";
import Markdown from "/components/markdown";

export default () => {
  return (
    <>
      <Head>
        <title>效能調整和FPS計數器</title>
        <meta name="description" content="FPS" />
      </Head>
      <Main>
        <Markdown
          content={`
尼爾：自動人形以前的版本好像有什麼全螢幕的解析度問題，發售快四年在2021年才修正。修正前，有人做過一個叫做FAR (Fix Automata Resolution，修好自動人形解析度)的模組。現在這個模組已經不需要了。

後來FAR與一個叫做Special K的專案合併。Special K可以當成是一個同時適用於一堆遊戲的模組，它基本上提供比遊戲內更多的畫質設定，還有一個FPS計數器。

Special K 有兩種模式：Global (全域) 是像是Xbox Game Bar一樣，安裝在系統上之後會出現在所有有支援的遊戲裡；Local (本地) 就跟一般的模組一樣，把一個dll放到遊戲的exe旁邊就好。

安裝前要先確保有打開遊戲過一次。

尼爾：自動人形基本上需要用本地模式。到 [Special K 的說明](https://wiki.special-k.info/en/SpecialK/Local)，找到 "Download Special K from the forum"，下載 test version 的 Archive 版 (對，太多沒有意義的選擇了)，然後解壓縮之後把 SpecialK64.dll 放在遊戲資料夾 (NierAutomata.exe 旁邊)，並且把它改名為 dxgi.dll。

打開遊戲之後按 Ctrl + Alt + Backspace，就會出現 Special K (FAR) 的設定畫面了。
`}
        />
      </Main>
    </>
  );
};
