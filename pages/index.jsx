import Head from "next/head";
import Main from "/components/main";

export default function Home() {
  return (
    <>
      <Head>
        <title>uni Next</title>
        <meta
          name="description"
          content="2021a 商務網站程式設計實作 期末作業"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Main>{/* index has nothing in the content area */}</Main>
    </>
  );
}
