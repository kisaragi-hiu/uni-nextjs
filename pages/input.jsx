import Head from "next/head";
import Main from "/components/main";
import Markdown from "/components/markdown";

export default () => {
  return (
    <>
      <Head>
        <title>Input</title>
        <meta name="description" content="Input" />
      </Head>
      <Main>
        <p>尼爾：自動人形的滑鼠與鍵盤輸入真的會讓人抓狂：</p>
        <ul>
          <li>
            2D模式中，遠距離攻擊的瞄準很不聽玩家的話：滑鼠稍微動一點瞄準點就會跳個50度左右。
          </li>
          <li>
            3D模式的攝影機把滑鼠當作是一個搖桿一樣，要瞄準永遠會有嚴重的延遲。如果這遊戲只有近戰就算了，但這是要怎麽瞄準？
          </li>
          <li>
            玩家會3D暈就是你攝影機不聽玩家的話，結果你是放一個NPC跟我講說「可以去設定調調看喔~」而不是讓攝影機直接跟著滑鼠。設定裡並沒有這個選項。
          </li>
          <li>
            閃躲 (evade) 在手把上只要按一個按鍵 (預設是R2)
            就好，但在滿滿都是按鍵的鍵盤上必須要連按兩下WASD。到底是為什麼？
          </li>
          <li>
            同理，鍵盤上滿是按鍵，你又不是像MMORPG一樣沒空間了，為什麼切換pod
            program要用<kbd>Alt+←</kbd>？
          </li>
          <li>
            快速目錄是<kbd>Delete</kbd>，然後<kbd>Tab</kbd>沒有用。
          </li>
        </ul>
        <Markdown
          content={`
按鍵設定還可以去設定裡改，但*Alt + ←*、只能雙擊閃躲、和攝影機延遲的問題真的只能安裝模組。

幸好的確有人做模組改善這些事情：NAIOM (NieR:Automata Input Overhaul Mod，尼爾：自動人形操作改善模組)。

NAIOM的主頁在[這裡](https://www.nexusmods.com/nierautomata/mods/12)。下載之後，把
dinput8.dll 和 NAIOM-GUI.exe 解壓縮到遊戲的資料夾，然後打開 NAIOM-GUI.exe 調整想要的設定。設定好之後打開遊戲，操作就不會像原本一樣痛苦了。
`}
        />
      </Main>
    </>
  );
};
