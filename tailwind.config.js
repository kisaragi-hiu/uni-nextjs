module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: { fontFamily: { sans: ["源柔ゴシック", "'Noto Sans CJK'"] } },
  },
  plugins: [require("@tailwindcss/typography")],
};
