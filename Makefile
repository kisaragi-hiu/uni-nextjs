dev: node_modules
	npx next dev

build: .next/BUILD_ID

export: .next/BUILD_ID
	npx next export

start: .next/BUILD_ID
	npx next start

.PHONY: dev build start export

node_modules: package.json
	npm install

SRC := $(shell find -regex ".*\(jsx\|js\|html\|css\)" -not -regex ".*\(\.next\|node_modules\).*")

# This allows build to not run if it doesn't need to
.next/BUILD_ID: node_modules $(SRC)
	npx next build
